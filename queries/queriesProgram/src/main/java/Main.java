import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        String rdfResourceDir;
        boolean experimentalMode = false;

        Scanner in = new Scanner(System.in);

        System.out.println("Run experiment? yes/no");
        while (in.hasNext()) {
            String answer = in.next();
            if (answer.equals("yes")) {
                experimentalMode = true;
                break;
            } else if (answer.equals("no")) {
                break;
            }
        }

        System.out.println("Write path to DIRECTORY with rdf resources:");
        if(in.hasNext()) {
            rdfResourceDir = in.next();

            App app = new App(experimentalMode);
            System.out.println("LOADING DATA...");

            if(!app.loadData(rdfResourceDir)){
                System.out.println("Invalid directory");
            }
            else {
                app.loadQueries(Queries.all);
                app.run();
            }
        }
    }
}
