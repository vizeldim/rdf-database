import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.mgt.Explain;
import org.apache.jena.util.FileManager;

import java.io.File;
import java.util.*;


public class App {
    private Model schoolModel;
    List<String> queryList = new ArrayList<>();
    private final boolean experimentalMode;

    public App(boolean mode){
        experimentalMode = mode;
    }

    public boolean loadData(String sourceDir){
        File dir = new File(sourceDir);
        File[] files = dir.listFiles((d, name) -> name.endsWith(".rdf"));

        if(files == null){
            return false;
        }
        schoolModel = FileManager.getInternal().loadModelInternal(files[0].getPath());
        for (int i = 1; i < files.length; ++i) {
            schoolModel.add(FileManager.getInternal().loadModelInternal(files[i].getPath()));
        }
        return true;
    }

    private void executeQuery(String queryString){
        Query query = QueryFactory.create(queryString);
        QueryExecution queryExecution = QueryExecutionFactory.create(query, schoolModel);

        if (experimentalMode){
            queryExecution.getContext().set(ARQ.symLogExec, Explain.InfoLevel.FINE) ;
        }
        else {
            System.out.println(queryString);
        }

        if (query.isSelectType())
        {
            ResultSet resultSet = queryExecution.execSelect();

            if (resultSet.hasNext()) {
                System.out.println("\nSolutions:");
                while (resultSet.hasNext()) {
                    QuerySolution querySolution = resultSet.nextSolution();
                    System.out.println(querySolution.toString());
                }
            } else {
                System.out.println("\nNo solution");
            }
        }
        else if (query.isAskType()) {
            System.out.println("\nAsk Solution = " + queryExecution.execAsk());
        }
        else if (query.isConstructType()) {
            System.out.println("\nCONSTRUCT solution:");
            queryExecution.execConstruct().write(System.out, "Turtle");

        }else if (query.isDescribeType()) {
            System.out.println("\nDESCRIBE solution:");
            queryExecution.execDescribe().write(System.out, "Turtle");
        }
        System.out.println();
    }

    public void run() {
        Scanner in = new Scanner(System.in);

        if(queryList.isEmpty()){ System.out.println("No query to execute"); }
        else {
            String msg = "Select query in range from = " + 1 + " to = " + queryList.size();
            System.out.println(msg);

            while (in.hasNext()) {
                int num;
                try {
                    num = in.nextInt();
                } // invalid input
                catch (InputMismatchException e) {
                    break;
                }

                if (num <= 0 || num > queryList.size()) // out of range
                    break;

                --num;
                executeQuery(queryList.get(num));
                System.out.println(msg);
            }
        }
    }


    public void loadQueries(String[] all)
    { queryList = Arrays.asList(all); }
}
