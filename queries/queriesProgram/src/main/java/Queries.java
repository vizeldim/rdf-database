public class Queries {
    // INTERNAL
    private static final String url              = "http://cvut.fit.vwm.vizeldim.hudebni-skola.cz";
    private static final String vocab            = "PREFIX vocab: <"            + url + "/vocab/>";
    private static final String adresa           = "PREFIX adresa: <"           + url + "/vocab/adresa/>";
    private static final String zak              = "PREFIX zak: <"              + url + "/vocab/zak/>";
    private static final String zamestnanec      = "PREFIX zamestnanec: <"      + url + "/vocab/zamestnanec/>";
    private static final String zakonny_zastupce = "PREFIX zakonny_zastupce: <" + url + "/vocab/zakonny_zastupce/>";
    private static final String pozice           = "PREFIX pozice: <"           + url + "/vocab/pozice/>";
    private static final String pujcka           = "PREFIX pujcka: <"           + url + "/vocab/pujcka/>";
    private static final String nastroj          = "PREFIX nastroj: <"          + url + "/vocab/hudebni_nastroj/>";
    private static final String vyrobce          = "PREFIX vyrobce: <"          + url + "/vocab/vyrobce/>";
    private static final String prodejce         = "PREFIX prodejce: <"         + url + "/vocab/prodejce/>";
    private static final String kategorie        = "PREFIX kategorie: <"        + url + "/vocab/kategorie/>";
    private static final String koncert          = "PREFIX koncert:  <"         + url + "/vocab/koncert/>";
    private static final String lekce            = "PREFIX lekce: <"            + url + "/vocab/lekce/>";
    private static final String predmet          = "PREFIX predmet: <"          + url + "/vocab/predmet/>";
    private static final String obor             = "PREFIX obor: <"             + url + "/vocab/obor/>";

    // EXTERNAL
    private static final String xsd              = "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>";
    private static final String foaf             = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>";


    /** 1
     * Seznam všech žáků(uri, jmeno), kteří bydlí mimo Prahu(mesto) a jejích jméno začíná na ‘I’
     * ( FILTER + REGEX )
     */
    public static final String q1 =
                    """
                    $zak
                    $vocab
                    $adresa
                    
                    SELECT ?zak ?jmeno ?mesto 
                    WHERE 
                    {
                        ?zak a vocab:zak ;
                                zak:jmeno ?jmeno ;
                                zak:adresa ?adresa .
                        ?adresa adresa:mesto ?mesto .
                        FILTER(?mesto != "Praha")
                        FILTER(REGEX( ?jmeno, "^I" ))
                    } 
                    
                    """
                            .replace("$zak", zak)
                            .replace("$vocab", vocab)
                            .replace("$adresa", adresa);



    /** 2
     * Vyber žáky(uri, jmeno), kteři mají půjčený(datum pujcky) alespoň jeden nástroj(nazev, cena, nazev_kategorie).
     * Výsledky seřaď dle ceny nástroje od nejvyšší. Omez výstup na nejvýše 3 záznamy.
     * ( ORDER BY + LIMIT )
     */
    public static final String q2 = """
                    $pujcka
                    $vocab
                    $nastroj
                    $zak
                    $kategorie
                    
                    SELECT ?zak ?jmeno ?nazev_kategorie ?nastroj_nazev ?datum ?cena
                    WHERE 
                    {
                        ?pujcka a vocab:pujcka ;
                                pujcka:zak ?zak ;
                                pujcka:hudebni_nastroj ?nastroj ;
                                pujcka:datum ?datum .
                        ?nastroj nastroj:nazev ?nastroj_nazev .
                        ?nastroj nastroj:cena ?cena .
                        ?nastroj nastroj:kategorie ?kategorie .
                        ?kategorie kategorie:nazev ?nazev_kategorie .
                        ?zak zak:jmeno ?jmeno
                    } 
                    ORDER BY DESC(?cena)
                    LIMIT 3
                    """
                        .replace("$zak", zak)
                        .replace("$vocab", vocab)
                        .replace("$pujcka", pujcka)
                        .replace("$nastroj", nastroj)
                        .replace("$kategorie", kategorie);


    /** 3
     * Najdi všechny zaměstnance(uri, jmeno), kteří pracují POUZE na pozici ‘učitel’
     * (MINUS)
     */
    public static final String q3 = """
                    $pozice
                    $vocab
                    $zamestnanec
                    
                    SELECT ?zam ?jmeno
                    WHERE 
                    {
                        {
                            SELECT ?zam
                            WHERE
                            {
                                ?pozice a vocab:pozice ;
                                        pozice:nazev "učitel" ;
                                        pozice:zamestnanec ?zam .
                            }
                        }
                        MINUS 
                        {
                            SELECT ?zam
                            WHERE
                            {
                                ?pozice a vocab:pozice ;
                                    pozice:nazev ?nazev .
                                    FILTER(?nazev!="učitel") .
                                ?pozice pozice:zamestnanec ?zam
                            }
                        }     
                        ?zam zamestnanec:jmeno ?jmeno .
                    } 
                    """
                        .replace("$vocab", vocab)
                        .replace("$pozice", pozice)
                        .replace("$zamestnanec", zamestnanec);


    /** 4
     * 3 koncerty s největším počtem účastníků. (uri koncertu, datum, nazev, počet účastníků)
     * (GROUP BY + count())
    */
    public static final String q4 = """
                    $vocab
                    $koncert
                    
                    SELECT ?koncert ?nazev ?datum (count(?ucastnik) as ?pocet_ucastniku)
                    WHERE 
                    {
                        ?koncert a vocab:koncert ;
                                koncert:nazev ?nazev ;
                                koncert:ucastnik ?ucastnik ;
                                koncert:datum ?datum .
                    } 
                    GROUP BY ?koncert ?nazev ?datum
                    ORDER BY DESC(?pocet_ucastniku)
                    LIMIT 3
                    """
                        .replace("$vocab", vocab)
                        .replace("$koncert", koncert);


    /** 5
     * Maximální a minimální cena hudebního nástroje
     * (max(), min())
     */
    public static final String q5 = """
                    $vocab
                    $hudebni_nastroj
                    
                    SELECT (max(?cena) as ?max_cena) (min(?cena) as ?min_cena)
                    WHERE 
                    {
                        ?nastroj a vocab:hudebni_nastroj ;
                                nastroj:cena ?cena .
                    } 
                    """
                        .replace("$vocab", vocab)
                        .replace("$hudebni_nastroj", nastroj);




    /** 6
     * Vypiš žáky(uri, jméno, email), kteří se zúčastnili VŠECH koncertů v roce 2017
     * (FILTER NOT EXISTS)
     */

    public static final String q6 = """
                    $vocab
                    $koncert
                    $zak
                    $xsd
                            
                                    
                    SELECT ?zak ?jmeno ?email
                    WHERE 
                    {
                        ?zak a vocab:zak 
                        FILTER NOT EXISTS
                        {
                            ?koncert a vocab:koncert ;
                                    koncert:datum ?datum
                            FILTER(year(?datum) = 2017)
                            FILTER NOT EXISTS
                            {
                                ?koncert koncert:ucastnik ?zak
                            }
                        } .
                        ?zak zak:jmeno ?jmeno ;
                             zak:email ?email 
                    } 
                    """
                        .replace("$vocab", vocab)
                        .replace("$zak", zak)
                        .replace("$xsd", xsd)
                        .replace("$koncert", koncert);



    /** 7
     * Ověření předchozího dotazu - Seznam všech koncertů v roce 2017 mínus seznam koncertů v roce 2017,
     * kterých se zúčastnili žáci, kteří jsou výstupem dotazu 6.
     * => musí být prázdná množina => Ask vrátí false
     */
    public static final String q7 = """
                    $vocab
                    $koncert
                    $zak
                    $xsd
                    
                    
                    ASK
                    {
                        {
                            SELECT ?koncert
                            WHERE
                            {
                                ?koncert a vocab:koncert ;
                                    koncert:datum ?dd
                                    FILTER(year(?dd)=2017)
                            }
                        }
                        MINUS
                        {
                            SELECT ?koncert
                            WHERE
                            {
                                {
                                    SELECT ?zak
                                    WHERE
                                    {        
                                        ?zak a vocab:zak
                                        FILTER NOT EXISTS
                                        {
                                            ?k a vocab:koncert ;
                                                koncert:datum ?d
                                            FILTER(year(?d)=2017)
                                            FILTER NOT EXISTS
                                            {
                                                ?k koncert:ucastnik ?zak
                                            }
                                        } 
                                    }
                                }
                                ?koncert koncert:ucastnik ?zak ;
                                        koncert:datum ?datum
                                        FILTER(year(?datum)=2017)       
                            } 
                        }
                    } 
                    """
                        .replace("$vocab", vocab)
                        .replace("$zak", zak)
                        .replace("$xsd", xsd)
                        .replace("$koncert", koncert);


    /** 8
     * Počet žáků, kteří chodí na lekce předmětu ‘Hra na klavír’
     * (count distinct)
     */
    public static final String q8= """
                    $vocab
                    $zak
                    $predmet
                    $lekce
                  
                    SELECT (count(distinct ?zak) as ?pocet_zaku_hra_na_klavir)
                    WHERE
                    {
                        ?lekce a vocab:lekce ;
                            lekce:predmet ?predmet .
                        ?predmet predmet:nazev "Hra na klavír" .
                        ?lekce lekce:zak ?zak
                    } 
                    """
                        .replace("$vocab", vocab)
                        .replace("$zak", zak)
                        .replace("$lekce", lekce)
                        .replace("$predmet", predmet);



    /** 9
     * Počet předmětů v jednotlivých oborech (existují nové obory, které zatím nemají předměty), seřezené dle počtu sestupně.
     * (OPTIONAL)
     */
    public static final String q9= """
                    $vocab
                    $obor
                    $predmet
                    $xsd

                  
                    SELECT ?obor ?nazev (count(?predmet) as ?predmet_pocet)
                    WHERE
                    {
                        ?obor a vocab:obor .
                         OPTIONAL{ ?predmet predmet:obor ?obor } .
                         ?obor obor:nazev ?nazev
                    } 
                    GROUP BY ?obor ?nazev
                    ORDER BY DESC(?predmet_pocet)
                    """
                        .replace("$vocab", vocab)
                        .replace("$obor", obor)
                        .replace("$predmet", predmet)
                        .replace("$xsd", xsd);



    /** 10
     * DESCRIBE - popiš kategorii s názvem klavír a všechny trojice,
     * v níchž je daná kategorie obsažena jako object (subject - predicate - object)
     */
    public static String q10 =
            """
            $kategorie
            
            
            DESCRIBE * WHERE
            {              
                ?klavir kategorie:nazev "Akordeon"@cs-CZ .
                ?s ?p ?klavir
            }
            """
                    .replace("$kategorie", kategorie);



    /** 11
     * CONSTRUCT - vytvoří nový model -> pro zaměstnance pracujicí na pozici:
     * “asistent pedagoga” nebo “zástupce ředitele” nebo “ředitel”
     * (Každý bude mít nasledujici nové property: ‘foaf:name’, ‘foaf:mbox’)
     * (CONSTRUCT, VALUES)
     */
    public static String q11 =
            """
                $vocab
                $zamestnanec            
                $pozice          
                $foaf
                
                CONSTRUCT
                {
                    ?zamestnanec foaf:name ?jmeno ;
                        foaf:mbox ?email .
                }
                WHERE
                {
                    SELECT distinct ?zamestnanec ?jmeno ?email
                    WHERE
                    {
                        VALUES ?nazev_pozice {"asistent pedagoga" "zástupce ředitele" "ředitel"}
                        ?pozice a vocab:pozice ;
                            pozice:nazev ?nazev_pozice ;
                            pozice:zamestnanec ?zamestnanec .
                        ?zamestnanec zamestnanec:jmeno ?jmeno ;
                            zamestnanec:email ?email
                    }
                }
                
                """
                    .replace("$vocab", vocab)
                    .replace("$zamestnanec", zamestnanec)
                    .replace("$pozice", pozice)
                    .replace("$foaf", foaf);




    /** 12
     * Seskup názvy všech oborů
     * (GROUP_CONCAT)
     */
    public static final String q12 =
            """
            $vocab
            $obor
            
            SELECT (GROUP_CONCAT(?nazev; separator = ",") as ?obory)
            WHERE {
                ?obor a vocab:obor ;
                    obor:nazev ?nazev
            }
                                
            """
                    .replace("$vocab", vocab)
                    .replace("$obor", obor);



    /** 13
     * ASK - Pozitivní dotaz(true): Existuje taková kategorie nástrojů, kde je průměrná cena za nástroj větší než 10000?
     * (isNumeric(), avg())
     */
    public static final String q13 =
            """
            $vocab
            $nastroj
            $kategorie
            $xsd
            
            ASK
            {
                SELECT ?kategorie
                WHERE
                {
                    ?kategorie a vocab:kategorie .
                    ?nastroj a vocab:hudebni_nastroj ;
                        nastroj:kategorie ?kategorie ;
                        nastroj:cena ?cena ;
                        FILTER(isNumeric((xsd:decimal(?cena)))) .
                }
                GROUP BY ?kategorie
                HAVING(avg(xsd:decimal(?cena)) > 10000) 
            }   
            """
                    .replace("$vocab", vocab)
                    .replace("$nastroj", nastroj)
                    .replace("$xsd", xsd)
                    .replace("$kategorie", kategorie);



    /** 14
     * Vypiš druhou stránku prodejců(nazev, optional(email, telefon, web)) obsahujicí 5 záznamů seřazených dle názvu.
     * (tzn. stránka obsahuje 5 záznamů) (LIMIT + OFFSET)
     */
    public static final String q14 =
            """
                    $vocab
                    $prodejce

                    SELECT ?prodejce ?nazev ?email ?tel ?web
                    WHERE 
                    {
                        {
                          SELECT ?prodejce ?nazev
                          WHERE 
                          {
                            ?prodejce a vocab:prodejce ;
                                 prodejce:nazev ?nazev .
                          } 
                          ORDER BY ?nazev 
                          LIMIT 5
                          OFFSET 5
                        }
                        OPTIONAL { ?prodejce prodejce:email ?email }
                        OPTIONAL { ?prodejce prodejce:tel ?tel }
                        OPTIONAL { ?prodejce prodejce:web ?web }
                    }
                    """
                    .replace("$vocab", vocab)
                    .replace("$prodejce", prodejce);



    /** 15
     * Seznam nástrojů(uri, nazev = model, cena) zakoupených u prodejce,
     * jehož web je 'www.muziker.cz' od výrobce 'Chesbro Music'.
     * Ke každému vypiš nazev kategorie, do které patři.
     * (Zkratka pomocí lomítka /)
     */

    public static final String q15= """
                    $vocab
                    $nastroj
                    $prodejce
                    $vyrobce
                    $kategorie
                    
                    SELECT ?nastroj ?nazev ?nazev_kategorie ?cena 
                    WHERE
                    {
                        ?prodejce a vocab:prodejce ;
                            prodejce:web "www.muziker.cz" .
                        ?nastroj a vocab:hudebni_nastroj ;
                            nastroj:prodejce ?prodejce ;
                            nastroj:kategorie / kategorie:nazev ?nazev_kategorie ;
                            nastroj:vyrobce / vyrobce:nazev 'Chesbro Music' ;
                            nastroj:nazev ?nazev ;
                            nastroj:cena ?cena .
                    } 
                    """
            .replace("$vocab", vocab)
            .replace("$nastroj", nastroj)
            .replace("$kategorie", kategorie)
            .replace("$prodejce", prodejce)
            .replace("$vyrobce", vyrobce)
            .replace("$xsd", xsd);



    /** 16
     * Jaké emailové domény používají žáci a zákonní zástupci?
     * (BIND + strafter() + BLANK NODE + zkratka pomocí | )
     */
    public static final String q16 =
            """
            $zak
            $zakonny_zastupce
 
            SELECT distinct ?domena
            WHERE 
            {
                 [] zak:email | zakonny_zastupce:email ?email .
                 BIND(strafter(?email, "@") as ?domena)
            }
            """
                    .replace("$zakonny_zastupce", zakonny_zastupce)
                    .replace("$zak", zak);



    /** 17
     * Součet cen a počet všech zakoupených nástrojů v rámci jednotlivých kategorii(uri, nazev).
     * (Seřazené podle celkové ceny sestupně).
     * Vypiš pouze ty kategorie, u níchž je celková cena vyšší než 200000.
     * (GROUP BY + HAVING + ORDER BY)
     */
    public static final String q17 =
            """
            $vocab
            $nastroj
            $kategorie
            
            SELECT ?kategorie ?nazev (count(?nastroj) as ?pocet_nastroju) 
                                     (sum(?cena) as ?celkova_cena)
            WHERE 
            {
                 ?nastroj a vocab:hudebni_nastroj ;
                    nastroj:cena ?cena ;
                    nastroj:kategorie ?kategorie .
                 ?kategorie kategorie:nazev ?nazev
                 
            }
            GROUP BY ?kategorie ?nazev
            HAVING(?celkova_cena > 200000)
            ORDER BY DESC(?celkova_cena)
            """
                    .replace("$nastroj", nastroj)
                    .replace("$kategorie", kategorie)
                    .replace("$vocab", vocab);


    // ========================================== Experimentální sekce ================================================


    /** 18
     * Najdi délku nejdelšího popisu lekce
     * PRVNI FORMULACE - agregacni funcke max
     */
    public static final String q18 =
            """
            $vocab
            $lekce
                        
                                 
            SELECT (max(?delka) as ?maximalni_delka)
            WHERE
            {
                ?lekce a vocab:lekce ;
                   lekce:poznamka ?poznamka .
                BIND(strlen(?poznamka) as ?delka)
            }   
            """
                    .replace("$vocab", vocab)
                    .replace("$lekce", lekce);



    /** 19
     * Najdi délku nejdelšího popisu lekce
     * DRUHA FORMULACE - ORDER BY + LIMIT
     */
    public static final String q19 =
            """
            $vocab
            $lekce
                        
                                 
            SELECT (?delka as ?maximalni_delka)
            WHERE
            {
                ?lekce a vocab:lekce ;
                   lekce:poznamka ?poznamka .
                BIND(strlen(?poznamka) as ?delka)
            } 
            ORDER BY DESC(?delka)
            LIMIT 1  
            """
                    .replace("$vocab", vocab)
                    .replace("$lekce", lekce);



    /** 20
     *  Žáci, kteří se nikdy nezúčastnili žádného koncertu
     *  PRVNI FORMULACE - pomocí NOT EXISTS
     */
    public static final String q20 =
                    """
                    $vocab
                    $koncert
                    $zak
                    $xsd

                  
                    SELECT ?zak ?jmeno ?email
                    WHERE
                    {
                        ?zak a vocab:zak 
                        FILTER NOT EXISTS
                        {
                            ?koncert a vocab:koncert ;
                                koncert:ucastnik ?zak
                        }
                        ?zak zak:jmeno ?jmeno ;
                             zak:email ?email
                    }
                    """
                        .replace("$vocab", vocab)
                        .replace("$koncert", koncert)
                        .replace("$zak", zak)
                        .replace("$xsd", xsd);

    /** 21
     *  Žáci, kteří se nikdy nezúčastnili žádného koncertu
     *  DRUHA FORMULACE - pomocí MINUS
     */
    public static final String q21=
                    """
                    $vocab
                    $koncert
                    $zak
                    $xsd
                    
                  
                    SELECT ?zak ?jmeno ?email
                    WHERE
                    {
                        ?zak a vocab:zak 
                        MINUS
                        {
                            ?k a vocab:koncert ;
                                koncert:ucastnik ?zak
                        }
                        ?zak zak:jmeno ?jmeno ;
                            zak:email ?email
                    } 
                    """
                        .replace("$vocab", vocab)
                        .replace("$koncert", koncert)
                        .replace("$zak", zak)
                        .replace("$xsd", xsd);



    /** 22
     *  Žáci, kteří se nikdy nezúčastnili žádného koncertu
     *  TRETI FORMULACE - pomocí OPTIONAL + GROUP BY + HAVING
     */
    public static final String q22 =
                    """
                    $vocab
                    $koncert
                    $zak
                    $xsd
                    
                  
                    SELECT ?zak ?jmeno ?email 
                    WHERE
                    {
                        ?zak a vocab:zak ;
                            zak:email ?email ;
                            zak:jmeno ?jmeno .
                        OPTIONAL{
                            ?koncert a vocab:koncert .
                                ?koncert koncert:ucastnik ?zak
                        }
                    }
                    GROUP BY ?zak ?jmeno ?email
                    HAVING(count(?koncert) = 0)
                    """
            .replace("$vocab", vocab)
            .replace("$koncert", koncert)
            .replace("$zak", zak)
            .replace("$xsd", xsd);


    /** 23
     * Žáci (uri, jmeno), kteří se zúčastnili koncertů s názvem ‘Vánoční koncert’ dne ‘08.12.2018’
     * a zároveň se zúčastnili koncertů s názvem ‘Vánoční koncert’ dne ‘12.12.2019’. (Průnik = intersection)
     *
     * První formulace - pomoci UNION
     * – Najdu koncerty s názvem ‘Vánoční koncert’ dne ‘08.12.2018’ a zjistím, kteří žáci se jich účastnili.
     * (distinct, protože se žák mohl v daný den účastnit 2 koncertů s názvem ‘Vánoční koncert’)
     * – Stejně tak se vyhledají koncerty dne ‘12.12.2019’
     * – Výsledky obou selectů se spojí pomocí UNION a vyberou se pouze ti žáci, kteří mají ve výsledku 2 záznamy
     * (tzn. účastnili se obou koncertů)
     */

    public static final String q23 =
            """
               $vocab
               $zak
               $koncert
               $xsd
               
               
               SELECT ?zak ?jmeno
               WHERE 
               {
                    {
                        SELECT distinct ?zak
                        WHERE
                        {
                            ?koncert a vocab:koncert ;
                                koncert:ucastnik ?zak ;
                                koncert:nazev "Vánoční koncert" ;
                                koncert:datum  "2018-12-08"^^xsd:date.
                            ?zak a vocab:zak .
                        }
                    }
                    UNION
                    {
                        SELECT distinct ?zak
                        WHERE
                        {
                            ?koncert a vocab:koncert ;
                                koncert:ucastnik ?zak ;
                                koncert:nazev "Vánoční koncert" ;
                                koncert:datum  "2019-12-12"^^xsd:date.
                            ?zak a vocab:zak .             
                        }
                    }.
                    ?zak zak:jmeno ?jmeno   
               }
               GROUP BY ?zak ?jmeno
               HAVING(count(*) = 2)                       
               """
                    .replace("$vocab", vocab)
                    .replace("$koncert", koncert)
                    .replace("$zak", zak)
                    .replace("$xsd", xsd);


    /** 24
     *  Žáci (uri, jmeno), kteří se zúčastnili koncertů s názvem 'Vánoční koncert' dne '08.12.2018'
     *  a zároveň  se zúčastnili koncertů  s názvem 'Vánoční koncert' dne '12.12.2019'. (Průnik = intersection)
     *
     * - Druhá formulace
     * -- Ze všech koncertů se pomocí FILTER najdou ty, které mají název 'Vánoční koncert' a konaly se dne '08.12.2018' nebo '12.12.2019'
     * -- Pomocí GROUP BY dojde k seskupení(koncertů) dle žáka a pomocí HAVING se vyberou ti žácí, kteří se účastnili koncertů s názvem  'Vánoční koncert' v oba dny.
     **/
    public static final String q24 =
            """
               $vocab
               $zak
               $koncert
               $xsd
               
               
               SELECT ?zak ?jmeno
               WHERE 
               {
                    ?koncert a vocab:koncert ;
                        koncert:ucastnik ?zak ;
                        koncert:nazev "Vánoční koncert" ;
                        koncert:datum ?datum 
                        FILTER(?datum = "2018-12-08"^^xsd:date || ?datum = "2019-12-12"^^xsd:date)
                    ?zak a vocab:zak ;
                        zak:jmeno ?jmeno .
               }
               GROUP BY ?zak ?jmeno
               HAVING(count(distinct ?datum) = 2)                       
               """
                    .replace("$vocab", vocab)
                    .replace("$koncert", koncert)
                    .replace("$zak", zak)
                    .replace("$xsd", xsd);


    /** 25
     * Vypiš žáky mladší 9 let
     * - Prvni formulace - FILTER
     */
    public static final String q25 =
            """
            $vocab
            $zak
            $xsd
            
            SELECT ?zak ?jmeno ?vek
            WHERE 
            {
               ?zak a vocab:zak ;
                    zak:datum_narozeni ?datum_narozeni .
               BIND(xsd:date(NOW()) as ?dnes) 
               BIND( year(?dnes) - year(?datum_narozeni) - if(month(?dnes) < month(?datum_narozeni) || (month(?dnes) = month(?datum_narozeni) && day(?dnes) < day(?datum_narozeni)),1,0) as ?vek )
               FILTER(?vek < 9)
               ?zak zak:jmeno ?jmeno
            }
            """
                    .replace("$vocab", vocab)
                    .replace("$xsd", xsd)
                    .replace("$zak", zak);


    /**
     * 26.) Vypiš žáky mladší 9 let
     * - Druhá formulace - HAVING
     */
    public static final String q26 =
            """
            $vocab
            $zak
            $xsd
            
            SELECT ?zak ?jmeno ?vek
            WHERE 
            {
               ?zak a vocab:zak ;
                    zak:datum_narozeni ?datum_narozeni .
               BIND(xsd:date(NOW()) as ?dnes) 
               BIND( year(?dnes) - year(?datum_narozeni) - if(month(?dnes) < month(?datum_narozeni) || (month(?dnes) = month(?datum_narozeni) && day(?dnes) < day(?datum_narozeni)),1,0) as ?vek )
               ?zak zak:jmeno ?jmeno
            }
            HAVING(?vek < 9)
            """
                    .replace("$vocab", vocab)
                    .replace("$xsd", xsd)
                    .replace("$zak", zak);




    public static final String[] all = {q1, q2, q3, q4, q5, q6, q7, q8, q9, q10,
                                        q11, q12, q13, q14, q15, q16, q17, q18, q19, q20,
                                        q21, q22, q23, q24, q25, q26};

}
